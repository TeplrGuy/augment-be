from flask import Flask, request, jsonify, send_from_directory
from flask_cors import CORS
from logging import FileHandler, WARNING
from PIL import Image
from werkzeug.utils import secure_filename
import shlex
import uuid
import subprocess
import os

# Statics
UPLOAD_MIDIS_FOLDER = 'files//uploaded/midis'
UPLOAD_PHOTO_FOLDER = 'files//uploaded/thumbnails'
UPLOAD_VIDEO_FOLDER = 'files//uploaded/videos'
UPLOAD_PROFILE_IMAGE_FOLDER = 'files//uploaded/pimages'

ALLOWED_EXTENSIONS = set(['mp3', 'wav', 'mp4', 'mov'])
ALLOWED_EXTENSIONS_IMAGES = set(['png', 'jpg', 'jpeg'])
ALLOWED_EXTENSIONS_MIDI = set(['mid', 'midi'])


FFMPEG_BIN = "/usr/bin/ffmpeg"
XAPI_Key = "dhuelyjso2dlyj3d0"


app = Flask(__name__)
CORS(app)
app.config['UPLOAD_MIDIS_FOLDER'] = UPLOAD_MIDIS_FOLDER
app.config['UPLOAD_PHOTO_FOLDER'] = UPLOAD_PHOTO_FOLDER
app.config['UPLOAD_VIDEO_FOLDER'] = UPLOAD_VIDEO_FOLDER
app.config['UPLOAD_PROFILE_IMAGE_FOLDER'] = UPLOAD_PROFILE_IMAGE_FOLDER

file_handler = FileHandler('errorlog.txt')
file_handler.setLevel(WARNING)

app.logger.addHandler(file_handler)


@app.route('/')
def welcome():
    return "Augment API"

# Allowed Midi Extensions
def allowed_midi_file(filename):
    # Build the filename + extension after checking if allowed
    # Note: \ == explicit line continuation
    return '.' in filename and \
        filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS_MIDI


# Allowed Image Extensions
def allowed_image_file(filename):
    # Build the filename + extension after checking if allowed
    # Note: \ == explicit line continuation
    return '.' in filename and \
        filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS_IMAGES


# Allowed Video Extensions
def allowed_file(filename):
    # Build the filename + extension after checking if allowed
    # Note: \ == explicit line continuation
    return '.' in filename and \
        filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

# Delete A Post
@app.route('/api/v1/uploads/midi/delete/<file_name>', methods=['POST'])
def uploaded_midi_delete_file(file_name):
    headers = request.headers
    auth = headers.get("X-Api-Key")

    if auth == XAPI_Key and request.method == 'POST':

        midiName = file_name + ".wav.midi"
        thumbName = file_name + ".png"
        videoName = file_name + ".mp4"


        if os.path.exists(os.path.join(app.config['UPLOAD_MIDIS_FOLDER'], secure_filename(midiName))):
            os.remove(os.path.join(app.config['UPLOAD_MIDIS_FOLDER'], secure_filename(midiName)))
        if os.path.exists(os.path.join(app.config['UPLOAD_PHOTO_FOLDER'], secure_filename(thumbName))):
            os.remove(os.path.join(app.config['UPLOAD_PHOTO_FOLDER'], secure_filename(thumbName)))
        if os.path.exists(os.path.join(app.config['UPLOAD_VIDEO_FOLDER'], secure_filename(videoName))):
            os.remove(os.path.join(app.config['UPLOAD_VIDEO_FOLDER'], secure_filename(videoName)))

        return jsonify({"response": "Successfully Deleted the midi, thumbnail, and video"})
    else:
        return jsonify({"response": "Access Denied"})

# Fetch a midi file
@app.route('/api/v1/uploads/midi/<file_name>', methods=['GET'])
def uploaded_midi_file(file_name):
    if request.method == 'GET':
        return send_from_directory(app.config['UPLOAD_MIDIS_FOLDER'], secure_filename(file_name))
    else:
        return jsonify({"response": "Access Denied"})

# Fetch a thumbnail image
@app.route('/api/v1/uploads/photo/<file_name>', methods=['GET'])
def uploaded_photo_file(file_name):
    if request.method == 'GET':
        return send_from_directory(app.config['UPLOAD_PHOTO_FOLDER'], secure_filename(file_name))
    else:
        return jsonify({"response": "Access Denied"})

# Fetch a video file
@app.route('/api/v1/uploads/video/<file_name>', methods=['GET'])
def uploaded_video_file(file_name):
    if request.method == 'GET':
        return send_from_directory(app.config['UPLOAD_VIDEO_FOLDER'], secure_filename(file_name))
    else:
        return jsonify({"response": "Access Denied"})

# Fetch a profile image file
@app.route('/api/v1/uploads/pimage/<file_name>', methods=['GET'])
def uploaded_pimage_file(file_name):
    if request.method == 'GET':
        return send_from_directory(app.config['UPLOAD_PROFILE_IMAGE_FOLDER'], secure_filename(file_name))
    else:
        return jsonify({"response": "Access Denied"})

# Upload a profile image
@app.route('/api/v1/profile/image', methods=['POST'])
def upload_profile_image():
    headers = request.headers
    auth = headers.get("X-Api-Key")
    user_id = headers.get("User-Id")

    if auth == XAPI_Key and request.method == 'POST' and user_id:
        # files

        # check if the post request has the file part
        if 'file' not in request.files:
            return jsonify({"response": "No File"})

        filePath = request.files['file']
        file_name_ext = secure_filename(filePath.filename)
        getFileExt = file_name_ext.split('.')[1]
        file_name = str(user_id) + "." + getFileExt
        fileJustName = file_name.split('.')[0]

        # check if image
        if filePath and allowed_image_file(file_name):
            # Write file to static directory
            if os.path.exists(os.path.join(app.config['UPLOAD_PROFILE_IMAGE_FOLDER'], secure_filename(file_name))):
                os.remove(os.path.join(app.config['UPLOAD_PROFILE_IMAGE_FOLDER'], secure_filename(file_name)))

            filePath.save(os.path.join(app.config['UPLOAD_PROFILE_IMAGE_FOLDER'], file_name))

            return jsonify({"response": "Profile Image was Successfully Uploaded"})
        else:
            return jsonify({"response": "File type not supported"})


    else:
        return jsonify({"response": "Can only make POST requests"})


# Upate Midi File
@app.route('/api/v1/midi/update', methods=['POST'])
def upload_midi_file():
    headers = request.headers
    auth = headers.get("X-Api-Key")

    if auth == XAPI_Key and request.method == 'POST':

        # check if the post request has the file part
        if 'file' not in request.files:
            return jsonify({"response": "No File"})

        filePath = request.files['file']
        file_name_ext = secure_filename(filePath.filename)
        file_name = file_name_ext

        # check if midi
        if filePath and allowed_midi_file(file_name):
            # Write file to static directory
            if os.path.exists(os.path.join(app.config['UPLOAD_MIDIS_FOLDER'], secure_filename(file_name))):
                os.remove(os.path.join(app.config['UPLOAD_MIDIS_FOLDER'], secure_filename(file_name)))

            filePath.save(os.path.join(app.config['UPLOAD_MIDIS_FOLDER'], file_name))

            return jsonify({"response": "Midi File was Successfully Uploaded"})
        else:
            return jsonify({"response": "File type not supported"})

    else:
        return jsonify({"response": "Can only make POST requests"})


# Upload a video for transcription
@app.route('/api/v1/transcribe/video', methods=['POST'])
def transcribe_video():
    headers = request.headers
    auth = headers.get("X-Api-Key")

    if auth == XAPI_Key and request.method == 'POST':
        # files

        # check if the post request has the file part
        if 'file' not in request.files:
            return jsonify({"response": "No Files"})


        filePath = request.files['file']
        file_name_ext = secure_filename(filePath.filename)
        getFileExt = file_name_ext.split('.')[1]


        file_name = str(uuid.uuid4()) + "." + getFileExt

        fileJustName = file_name.split('.')[0]

        destinationPath = "files/uploaded/midis/" + fileJustName + ".wav"
        destinationThumbPath = "files/uploaded/thumbnails/" + fileJustName + ".png"

        destination_file_name = fileJustName + ".wav.midi"
        file_name_wave = fileJustName + ".wav"


        if filePath and allowed_file(file_name):
            
            # Write file to static directory
            filePath.save(os.path.join(app.config['UPLOAD_VIDEO_FOLDER'], file_name))

            # Convert Video To Wav
            fileType = filePath.filename.rsplit('.', 1)[1].lower()

            fullPath = "files/uploaded/videos/" + file_name


            if fileType in ["mov", "mp4"]:
                # video to thumbnail
                commandImage = FFMPEG_BIN + " -i " + fullPath + " -s 400x400 -ss 00:00:01.000 -vframes 1 " + destinationThumbPath
                subprocess.call(commandImage, shell=True)


                # video to wav
                command = FFMPEG_BIN + " -i " + fullPath + " -ab 160k -ac 2 -ar 44100 -vn " + destinationPath
                subprocess.call(command, shell=True)


                # convert from wav to midi
                commandTranscribe = "onsets_frames_transcription_transcribe --hparams=use_cudnn=false --model_dir='checkpoints/train' " + destinationPath
                subprocess.call(commandTranscribe, shell=True)


                # Delete file when done with transforming to wav
                if os.path.exists(os.path.join(app.config['UPLOAD_MIDIS_FOLDER'], file_name_wave)):
                    os.remove(os.path.join(app.config['UPLOAD_MIDIS_FOLDER'], file_name_wave))

                return jsonify({
                    "response": "Successfully uploaded video",
                    "fileName": file_name
                })

            else:
                return jsonify({"response": "File type not supported"})
        else:
            return jsonify({"response": "Outer File type not supported"})

    else:
        return jsonify({"response": "Can only make POST requests"})


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=3000)

