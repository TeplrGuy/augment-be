from flask import Flask, jsonify
from flask_pymongo import PyMongo 

app = Flask(__name__)
app.config["MONGO_URI"] = "mongodb://0.0.0.0:27017/augment"
mongo = PyMongo(app)

@app.route("/<ObjectId:id>")
def home_page(id):
    user_object = mongo.db.users.find({"_id":id})
    print(user_object)
    return jsonify({"response": "Can only make POST requests"})


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=3000, debug=True)
