FROM python:3.6.9
WORKDIR /app
RUN apt-get update -y && \
    apt-get install -y python3-dev python3-pip

RUN pip3 install --upgrade pip
COPY requirements.txt requirements.txt 
RUN pip3 install -r requirements.txt

RUN apt-get update -y 
RUN apt-get install -y libsndfile1 && \
    apt-get install -y libsndfile1-dev && \
    apt-get install -y ffmpeg

COPY . /app
EXPOSE 3000
ENTRYPOINT [ "python3" ]
CMD [ "app.py" ]